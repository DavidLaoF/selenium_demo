# Selenium Demo



## Getting started

1- Add Selenium in Python Interpreter


2- To ensure the proper functionality  of the HtmlTestRunner library, it was necessary to modify the code in the result.py file from 

	length = self._count_relevant_tb_levels(tb)
    msg_lines = traceback.format_exception(exctype, value, tb, length)
	
to 

	msg_lines = traceback.format_exception(exctype, value, tb)
	
This change was made to accommodate the latest updates and compatibility requirements with the Python3 environment.
Please make sure to update the respective code in the result.py file for seamless integration and accurate test report generation using HtmlTestRunner