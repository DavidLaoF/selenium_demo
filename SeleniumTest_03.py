import unittest
import HtmlTestRunner

def addition(x, y , z=0):
 return x+y+z


class AddTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_Equal(self):
        self.assertEqual(addition(10,11,11),33)

    def test_NotEqual(self):
        self.assertNotEqual(addition(10,11), 44)

    def tearDown(self):
        pass

if __name__ == "SeleniumTest_03":
    suite = unittest.TestLoader().loadTestsFromTestCase(AddTest)

    runner = HtmlTestRunner.HTMLTestRunner(output='reports')

    runner.run(suite)