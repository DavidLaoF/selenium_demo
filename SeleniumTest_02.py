from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

print("Inicio del Test")

driver = webdriver.Chrome()
driver.maximize_window()
driver.delete_all_cookies()

driver.get("https://www.google.com/")
time.sleep(2)

try:
    reject_button = driver.find_element("id", "L2AGLb")
    reject_button.click()
    time.sleep(2)
except Exception as e:
    print(e)

search_box = driver.find_element("id","APjFqb")
search_box.send_keys("Test Selenium Boton")
time.sleep(2)

search_button = driver.find_element("name", "btnK")
search_button.click()
time.sleep(5)

driver.quit()
